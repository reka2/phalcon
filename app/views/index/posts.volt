{% for post in posts %}

	<span style="font-size:10px; color:#8F8F8F" class="pull-right" id="time{{ post.id }}"></span>
	<strong>
		{{ post.title }}
	</strong> 
 		
	<br>
	{{ post.content }}
	<br>
		<a class="label label-success" href="/index/editpost/{{ post.id }}">
		<i class="fa fa-pencil-square"></i> edit
		</a>
		<a style="margin-left:1em;" onClick="confirmDelete({{ post.id }})" href="#" class="label label-danger">
			<i class="fa fa-times"></i> delete
		</a>

	<hr>


	<script type="text/javascript">
	var waktu = moment("{{ post.created_at }}").fromNow();
	$('#time'+{{ post.id }}).html(waktu);
	</script>

{% endfor %}


<script type="text/javascript">
function confirmDelete(id){

	swal({
		title : 'are you sure ?',
		type  : 'warning',
		closeOnCancel: true,
		showCancelButton: true,
		closeOnConfirm: false,
		showLoaderOnConfirm : true
		
	}, function(isConfirm){
		if(isConfirm){
			$.ajax({
				url : '/index/deletepost/'+id,
				data : {},
				type : 'get',
				error: function(err){
					swal('error', 'terjadi kesalahan pada sisi server!', 'error');
				},
				success:function(ok){
					swal({
					title : "success!", 
					text : "data telah terhapus!", 
					type : "success"
					}, function(){
						window.location.reload();
					})
				}
			})	
		}
		return false;
	});

}
</script>

 
 
