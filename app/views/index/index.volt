<div class="page-header">
    <h1> <i class="fa fa-newspaper-o"></i> Latest Article!</h1>
</div>


 
 <div class="row">
 	<div class="col-md-6">

 		
 		<h3>
 		<i class="fa fa-plus-square"></i> Insert Data
 		</h3>
 		<hr>
 		<div class=”flash-message-css”><?php $this->flashSession->output(); ?></div>


 			<form action="/index/insert" method="post">

				<div class="form-group">
			    <label for="title">Title : </label>
				    {{ text_field("title", "size": 32, "class": "form-control", "placeholder": "title...", "id" : "title") }}
				</div>
				<div class="form-group">
					<label for="content">Content : </label>
					<textarea style="height: 10em;" class="form-control" name="content" id="content" placeholder="content..."></textarea>
				</div>

				<div class="form-group">
				     <button type="submit" id='simpan' class='btn btn-info'><i class='fa fa-floppy-o'></i> SAVE</button>			
				</div>
				</form>

 	</div>
 	<div class="col-md-5 col-md-offset-1">
	 	<?php $this->partial("index/posts"); ?>
 	</div>
 </div>

 


