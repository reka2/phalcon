<div class="page-header">
    <h1> <i class="fa fa-newspaper-o"></i> Article!</h1>
</div>


 
 <div class="row">
 	<div class="col-md-6">

 		<h3>
 		<i class="fa fa-pencil-square"></i> Edit Data
 		</h3>
 		<hr>
 		{{ flash.output() }}

 		
 			<form action="/index/updatepost" method="post">

				<div class="form-group">
			    <label for="title">Title : </label>
				    {{ text_field("value": post.title,  "title", "size": 32, "class": "form-control", "placeholder": "title...", "id" : "title") }}
				</div>
				<div class="form-group">
					<label for="content">Content : </label>
					<textarea style="height: 10em;" class="form-control" name="content" id="content" placeholder="content...">{{ post.content }}</textarea>
				</div>
					<input type="hidden" name="id" value="{{ post.id }}">

				<div class="form-group">
				     <button type="submit" id='simpan' class='btn btn-info'><i class='fa fa-floppy-o'></i> SAVE</button>			
					<a href="/" class="btn btn-default" style="margin-left:1em;">
						  Back
					</a>

				</div>
				</form>

 	</div>
 </div>

 


