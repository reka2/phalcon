<?php
use Phalcon\Http\Response;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->posts = Posts::find();
    }

 

    public function insertAction()
    {
        $post = new Posts();
        $post->content = $this->request->getPost('content');
        $post->title = $this->request->getPost('title');
 
        if(!$post->save()){
            $this->flash->error("gagal disimpan");
        }else{
            $this->flash->success("Berhasil di simpan!");
        }        
        $this->response->redirect('/');

    }

    public function editpostAction($postId)
    {
        $this->view->posts = Posts::find();
        $post = Posts::findFirst($postId);
        $this->view->post = $post;
    }

    public function updatepostAction()
    {
        $post = Posts::findFirst($postId);
        $post->content = $this->request->getPost('content');
        $post->title = $this->request->getPost('title');

        if(!$post->update()){
            $this->flash->error("gagal disimpan");
        }else{
            $this->flash->success("Berhasil di simpan!");
        }        
        $this->response->redirect('/');        
    }

    public function deletepostAction($postId)
    {
        $post = Posts::findFirst($postId);
        $post->delete();
    }

}

